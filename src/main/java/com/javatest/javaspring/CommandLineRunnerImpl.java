package com.javatest.javaspring;

import com.javatest.javaspring.config.Item;
import com.javatest.javaspring.config.ProductConfig;
import com.javatest.javaspring.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

/**
 *
 *  Here we run the application as CommandLineRunner
 *  Initial messages and validation messages are printed here.
 * @author Konstantin Kadin
 *
 */

@Component
public class CommandLineRunnerImpl implements CommandLineRunner {

    @Autowired
    private ProductConfig productConfig;

    @Autowired
    private ProductServices productServices;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("\nWelcome to our Grocery Shop!\n\n");
        String typedElement = "";

        // loop controls the application run process
        while (!typedElement.equals("exit")) {
            List<Item> itemList = productConfig.getItem();
            productServices.printPriceList();
            Scanner scanner = new Scanner(System.in);

            // here we read the inputs
            System.out.print("\nPriceBasket ");
            typedElement = scanner.nextLine().toLowerCase();
            // split the string, throw in array
            String[] parsedItems = typedElement.split("\\s+");

            // validate inserted values
            int countIterations = 0;
            for (String parsedItem : parsedItems) {
                for (Item item : itemList) {
                    if (parsedItem.equals(item.getName())) {
                        countIterations++;
                    }
                }
            }

            if (countIterations == parsedItems.length) {
                productServices.calculateTotal(parsedItems);
            } else {
                System.out.println("\n-------------------------------------------------\n" +
                        "| Only items from the Price List are available! |\n" +
                        "|                  Please try again!            |\n" +
                        "-------------------------------------------------\n\n");
            }
            System.out.println("\nPress enter to start again, or type 'exit' to exit the application:");
            typedElement = scanner.nextLine();
            System.out.println("\n\n");
        }
    }
}
