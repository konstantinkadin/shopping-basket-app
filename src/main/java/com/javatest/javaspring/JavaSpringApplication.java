package com.javatest.javaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Here we disable tomcat web server which is configured by default in Spring.
 * @author Konstantin Kadin
 *
 */

@SpringBootApplication
public class JavaSpringApplication {

    public static void main(String[] args) {
        SpringApplication springApplication =
                new SpringApplicationBuilder(JavaSpringApplication.class)
                        .web(WebApplicationType.NONE)
                        .build();

        springApplication.run(args);
    }
}
