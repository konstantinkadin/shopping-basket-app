package com.javatest.javaspring.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
*
* This is where we bind configurations in application.properties file to POJOs.
* @author Konstantin Kadin
*
*/

@Configuration
@EnableConfigurationProperties(ProductConfig.class)
public class AppConfig {
}
