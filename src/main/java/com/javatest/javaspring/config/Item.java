package com.javatest.javaspring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * Configurations per product/item are picked up here
 * @author Konstantin Kadin
 *
 */

@Component
@ConfigurationProperties(prefix = "product.item")
public class Item {

    private String name;
    private double price;
    private String unit;
    private boolean discountEnabled;
    @Nullable
    private String discountDependent;
    private int discountDependentItemsNumber;
    private int discountPercentage;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isDiscountEnabled() {
        return discountEnabled;
    }

    public void setDiscountEnabled(boolean discountEnabled) {
        this.discountEnabled = discountEnabled;
    }

    public String getDiscountDependent() {
        return discountDependent;
    }

    public void setDiscountDependent(String discountDependent) {
        this.discountDependent = discountDependent;
    }

    public int getDiscountDependentItemsNumber() {
        return discountDependentItemsNumber;
    }

    public void setDiscountDependentItemsNumber(int discountDependentItemsNumber) {
        this.discountDependentItemsNumber = discountDependentItemsNumber;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", unit='" + unit + '\'' +
                ", discountEnabled=" + discountEnabled +
                ", discountDependent='" + discountDependent + '\'' +
                ", discountDependentItemsNumber=" + discountDependentItemsNumber +
                ", discountPercentage=" + discountPercentage +
                '}';
    }

    // this method capitalizes the names, used mostly to print the items in console
    public String capitalizedName() {
        return getName().substring(0, 1).toUpperCase() + name.substring(1);
    }
}
