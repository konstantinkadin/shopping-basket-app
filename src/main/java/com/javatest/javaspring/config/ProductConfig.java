package com.javatest.javaspring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *  Configurations of all products are thrown here in ArrayList
 * @author Konstantin Kadin
 *
 */

@ConfigurationProperties(prefix = "product")
@EnableConfigurationProperties(Item.class)
public class ProductConfig {

    private String test;
    private List<Item> item = new ArrayList<>();

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> items) {
        this.item = items;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "ProductConfig{" +
                "test='" + test + '\'' +
                ", item=" + item +
                '}';
    }
}
