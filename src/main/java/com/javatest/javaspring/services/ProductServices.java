package com.javatest.javaspring.services;

import com.javatest.javaspring.config.Item;
import com.javatest.javaspring.config.ProductConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *  All the application total price calculation functionality is in this class
 * @author Konstantin Kadin
 *
 */

@Component
public class ProductServices {

    @Autowired
    private ProductConfig productConfig;

    public void printPriceList() {
        System.out.println("Price List of products:\n");
        List<Item> itemsList = productConfig.getItem();

        for (Item product : itemsList) {
            System.out.println("● " + product.capitalizedName() +
                    " - " + getCurrencyType(product.getPrice()) +
                    " per " + product.getUnit() + ".");
        }

        currentSpecialOffers(itemsList);
    }

    //this methods prints special offers
    public void currentSpecialOffers(List<Item> itemsList) {
        String specialOffers = "";
        for (Item item : itemsList) {
            if (item.isDiscountEnabled()){
                specialOffers+="\n";
                if (item.getDiscountDependent().isEmpty()){
                    specialOffers += "* " + item.capitalizedName() + " have a "
                    + item.getDiscountPercentage() + "% discount off their normal price this week.";
                } else {
                    specialOffers += "* Buy " + item.getDiscountDependentItemsNumber() +
                            " " + item.getUnit() + "s of " + item.getDiscountDependent() +
                            " and get a loaf of bread for half price.";
                }
            }
        }
        System.out.println(specialOffers);
    }

    // this method converts price value and attaches to it either pound or pence
    public String getCurrencyType(double price) {
        String priceToReturn = "";
        if (price > 1 || price == 1) {
            priceToReturn = "£" + String.format( "%.2f", price);
        } else if (price < 1) {
            String doubleConvertedToString = Double.toString(round(price*100, 2));
            priceToReturn = doubleConvertedToString.substring(0, doubleConvertedToString.length() - 2) + "p";
        }
        return priceToReturn;
    }

    // this method rounds a double value to the decimal places specified
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    // this method counts items in String array, then adds them to hashmap where key is item name and value is the number of
    // item occurrences
    public Map<String, Integer> countItems(String[] insertedItems) {
        // hashmap to store the frequency of element
        Map<String, Integer> coutedItems = new HashMap<String, Integer>();

        for (String i : insertedItems) {
            Integer j = coutedItems.get(i);
            coutedItems.put(i, (j == null) ? 1 : j + 1);
        }
        return coutedItems;
    }

    // all the logic for calculating total price is here
    public void calculateTotal(String[] insertedItems) {
        Map<String, Integer> countedBasketItems = countItems(insertedItems);
        String message = "";
        String noOffersMessage = "";
        double subTotal = 0;
        double total = 0;
        double savedMoneyFromProduct = 0;
        double productAfterDiscount = 0;
        for (String i : countedBasketItems.keySet()){
            for (Item item : productConfig.getItem()) {
                if (i.equals(item.getName())){
                    double numberItemsInBasket = Double.valueOf(countedBasketItems.get(i));
                    double nonDiscoutedPrice = item.getPrice() * numberItemsInBasket;
                    double discountedPricePerProduct= applyDiscounts(i, countedBasketItems);
                    subTotal += nonDiscoutedPrice;

                    // total price including discounts is calculated using discount dependency configured in application.properties

                    // total price where product's discounts has got dependency is calculated here
                    if (item.isDiscountEnabled() && !item.getDiscountDependent().isEmpty()) {
                        int numberOfProductsWithDiscount = (int)findNumberOfItems(countedBasketItems, item.getDiscountDependent()) / item.getDiscountDependentItemsNumber();
                        if (numberOfProductsWithDiscount>numberItemsInBasket){numberOfProductsWithDiscount = (int)numberItemsInBasket;}
                        int numberOfProductsWithoutDiscount = (int)numberItemsInBasket - numberOfProductsWithDiscount;
                            if(numberOfProductsWithDiscount >= 0) {
                                productAfterDiscount = numberOfProductsWithDiscount*discountedPricePerProduct + numberOfProductsWithoutDiscount * item.getPrice();
                                savedMoneyFromProduct = (numberOfProductsWithDiscount * item.getPrice()) - numberOfProductsWithDiscount*discountedPricePerProduct;
                                total = total + productAfterDiscount;
                            }
                    } else {
                        // total price where product's discounts has got no dependency is calculated here
                        // total price of the products without discounts also calculated here
                        productAfterDiscount = discountedPricePerProduct * numberItemsInBasket;
                        savedMoneyFromProduct = (item.getPrice() * numberItemsInBasket) - productAfterDiscount;
                        total = total + productAfterDiscount;
                    }

                    if (item.isDiscountEnabled()){
                        if (savedMoneyFromProduct != 0){
                            message += item.capitalizedName() + " "
                                    + item.getDiscountPercentage() + "% off: "
                                    + getCurrencyType(savedMoneyFromProduct) + "\n";
                        }
                    }
                    break;
                }
            }
        }
        printTotalPriceMessages(message, noOffersMessage, subTotal, total);
    }

    // code prints the total price and subtotal price messages
    public void printTotalPriceMessages(String message, String noOffersMessage, double subTotal, double total) {
        if(subTotal == total){noOffersMessage = " (No offers available)";}
        System.out.println("\n----------------------------" +
                "\nSubtotal: " + getCurrencyType(subTotal) + noOffersMessage);
        if (!message.isEmpty()) {
            System.out.println("\n" + message);
        } else System.out.println("\n");
        System.out.println("Total price: " + getCurrencyType(total) +
                "\n----------------------------");
    }

    // calculates discounted price per product
    public double applyDiscounts(String itemToApplyDiscount, Map<String, Integer> numberOfItems) {
        double discountedPrice = 1;
        for (Item item : productConfig.getItem()) {
            if (itemToApplyDiscount.equals(item.getName())) {
                discountedPrice = item.getPrice();
                if (item.isDiscountEnabled()) { //if item discount is true
                    if (item.getDiscountDependent().isEmpty()) {
                        discountedPrice = discountedPrice * ((100 - item.getDiscountPercentage()) * 0.01);
                    } else {
                        if (item.getDiscountDependentItemsNumber() <= findNumberOfItems(numberOfItems, item.getDiscountDependent())) {
                            discountedPrice = discountedPrice * ((100 - item.getDiscountPercentage()) * 0.01);
                        }
                    }
                }
            }
        }
        return discountedPrice;
    }

    // method returns number of specific items per product name in hashmap
    public int findNumberOfItems(Map<String, Integer> countedItems, String itemName) {
            int numberOfItems = 0;
            for (String i : countedItems.keySet()){
                if (i.equals(itemName)) {
                    numberOfItems = countedItems.get(i);
                }
            }
            return numberOfItems;
    }
}
