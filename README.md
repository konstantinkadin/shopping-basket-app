Shopping Basket App 
===========

Description
-----------

Shopping Basket App is Java/Spring Boot application which takes items to be purchased as command line arguments separated from each other via space, then sums their total cost according to their pre-configured prices. 
Then it calculates the discounts applied to the purchased items and prints out a the subtotal, discount information and total.
In order to exit from the application it is sufficient to type 'exit' in "PriceBasket" field.
The application has been designed in completely flexible way so the user may add desired products and change configurations of existing products.

How to Run
----------

You must have Java installed and configured. Import the project into your IDE:
<pre>
https://gitlab.com/konstantinkadin/shopping-basket-app.git
</pre>
Run 'JavaSpringApplication' class

Implementation Details
----------------------
The application is written with Spring Boot Java framework.
The only external dependency I am using is the one to activate configuration processor.

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>

The configurations of the products are stored in application.properties file.
All the data reflected in console is dynamic and linked to the configuration file.

For example, these are the configurations for the product 'bread'
<pre>
product.item[1].name = bread
product.item[1].price = 0.80
product.item[1].unit = loaf
product.item[1].discount-enabled=true
product.item[1].discount-dependent=soup
product.item[1].discount-dependent-items-number=2
product.item[1].discount-percentage=50
</pre>

Configurations description:

- "product.item[1].name" - name of the product
- "product.item[1].price" - price of the product
- "product.item[1].unit" - unit measurement
- "product.item[1].discount-enabled" - here we mark the product as discount enabled (true/false)
- "product.item[1].discount-dependent" - here we define the discount dependency
- "product.item[1].discount-dependent-items-number" - the number of the items the discount is dependent on
- "product.item[1].discount-percentage" - discount percentage

If the discount is not dependant on any items it is sufficient to specify only "product.item[1].discount-enabled" and "product.item[1].discount-percentage" discount related configurations.
Items entered in "Price Basket" are case insensitive, then transformed in lowercase for further manipulation. 
Once the total price has been calculated, user has possibility to restart the app by pressing "enter" button, 
or exit by typing 'exit' in console.

Assumptions
-----------

Extra logic can be added for displaying Special Offers messages accordingly to the 'discount-dependent-items-number' configuration specified in application.properties file.



